# README #

### What is the content of this repository? ###

This repository contains the C++ code for the sequential deposition algorithm (https://doi.org/10.1103/PhysRevE.84.050302).
The algorithm finds the optimal packing fraction by sequentially dropping spheres inside a tube,
similar to the golf balls being dropped inside the transparent tube in the gif below.

![](Images/SpherePacking2.gif)

The code serves two purposes:

* To find and output a densest possible structure as obtained from a variation of the angular position θ,
  and in some cases also the vertical position z of the second sphere;
* To output a specific structure at given values of θ and z as constructed via the present packing algorithm.

A two-dimensional simulation is adopted for the angular and vertical dimensions of such a system,
where the spheres are all in touch with the cylindrical boundary.
Although this packing algorithm only considers densest possible columnar structures with cylinder-touching spheres,
the algorithm itself works for any value of D.
Therefore it can also be regarded as an independent surface-packing algorithm for generating a class of columnar structures
with only cylinder-touching spheres.


### How to execute the code? ###

* Compile `BkAppDCode.cpp` with C++ compiler of your choice (preferably gcc)
* Run the binary file without any parameters

### Requirements ###
No requirements necessary.

### Contact ###

* [Ho-Kei Chan](mailto:epkeiyeah@yahoo.com.hk)
* [Jens Winkelmann](jwinkelm@tcd.ie)