
// Simultaneous deposition of 'u' spheres at each deposition step.

#include <iostream>
#include <math.h>
#include <fstream>
#include <stdlib.h>
#include <time.h>

using namespace std;

// The code allows a search of densest possible structures across a finite range of D,
// which varies from MinD to (MinD + MaxCompStep x IncreD) at steps of IncreD.
// Note that MinD must be greater than unity.

double D;
const double MinD = 3.90;
const double IncreD = 0.0005;

int CompStep;
int MaxCompStep = 8;

// For any value of D, one can generally have a simultaneous deposition of u spheres at
// each deposition step, provided that u is no greater than an upper limit UppLimu.

const double u = 1;
double UppLimu;

// And corresponding to any input value of u is a specific range of the angular position
// Theta in the simulation array,

const double Pi = 3.1416;
const double MaxTheta = 2*Pi/u;
const double MaxThetaDeg = 360/u;

// as well as a corresponding lowest allowed value of D. Another variable is here defined
// for some trial values of u in a subsequent loop that sets the maximum number of spheres
// to be deposited into the cylinder.

double LowLimD, LowLimDTrial;

// For every angular position Theta, a one-dimensional array is adopted to store the lowest
// possible value of z where a sphere can be placed. The array size ArrSizeTheta and the range
// of Theta together determine the resolution of the angular dimension in the simulation. Here,
// ArrSizeTheta is defined to be an even number. An integer j, which ranges from 0 to
// (ArrSizeTheta - 1) and is to be defined subsequently in the code, is used to refer to any
// particular element in the array, i.e. any particular value of the angular position Theta,
// where MaxDELj is the value of j that corresponds to the angular position Theta = MaxTheta/2.

const int ArrSizeTheta = 25000;
const int MaxDELj = ArrSizeTheta/2;

// Two functions are defined for converting j into Theta, in radian and in degree respectively.

double ThetaConv (int j)
{
     return MaxTheta*((double)j/ArrSizeTheta);
}

double ThetaConvDeg (int j)
{
     return MaxThetaDeg*((double)j/ArrSizeTheta);
}

// For the vertical z dimension, two constant parameters are defined to set the scale of the
// simulation. They are, respectively, the maximum value of z adopted in the simulation as
// well as the corresponding number of steps discretized across this range of z:

const double Maxz = 15;
const int ArrSizez = 15000;

// An integer i, which ranges from 0 to (ArrSizez - 1) and is to be defined subsequently in
// the code, is used to refer to any particular z position. A function is defined to convert
// i into z according to the above scale.

double zConv (int i)
{
     return Maxz*((double)i/ArrSizez);
}

// Variables are defined for computations that involve the angular position Theta, including
// the position Theta2 of the second sphere:

double Theta, DELTheta, s;
int Minj, NewMinj, LowLimNewMinj, UppLimNewMinj;
int j, j2Trial, j2Output;
int DELj;
int MaxDELjNzDELzContact, DELjZrDELzContact;

// Two regimes of Theta are defined: In Regime 1 where allowed values of the integer j range
// from 0 to (DELjZrDELzContact - 1), two spheres separated by an angular displacement of Theta
// could be brought into mutual contact as one sphere is placed at a lowest z position relative
// to the other. In Regime 2 where values of j range from DELjZrDELzContact to MaxDELj, any two
// spheres separated by Theta would never be in touch with each other regardless of their vertical
// positions z. For an evaluation of the maximum packing density along the z dimension, i.e.
// maximum number of spheres per unit length, the following variables are defined to store the
// maximum packing density in each regime:

int LowLimj2MaxdNdzReg1, UppLimj2MaxdNdzReg1;

// for Regime 1, and

int LowLimj2MaxdNdzReg2, UppLimj2MaxdNdzReg2;
int LowLimj2MaxdNdzReg2i, UppLimj2MaxdNdzReg2i;
int j2MaxdNdzReg2i;

// for Regime 2. The overall maximum of this packing density is then stored in another variable:

int j2MaxdNdz;

// A number of variables are defined for computations that involve the vertical position z,

double z;
int Mini, PrevMini;
int ArrDELiContact[(ArrSizeTheta/2)+1];
int DELiContact;
double SqDELzContact;

int ArrLowesti[ArrSizeTheta];
int NewLowesti;

// and also for an evaluation of the maximum packing density in Regime 1

int LowLimi2MaxdNdzReg1, UppLimi2MaxdNdzReg1;

// and in Regime 2

int LowLimi2MaxdNdzReg2, UppLimi2MaxdNdzReg2;

// respectively. Variables are also defined for Regime 2 as related to a variation of the position
// z2 of the second sphere in the search for densest possible configurations:

int i2, i2Trial, i2Output;
const int Mini2Trial = 0;
const int Maxi2Trial = int(((double)ArrSizez)/Maxz);
int i2MaxdNdz;

// To avoid any possible inclusion of transient configurations in the evaluation of packing densities,
// a threshold of z is adopted where only spheres at z positions greater than this threshold are
// taken into account:

const double Thresholdz = 2;

// A choice of 2 for this threshold is sufficient for most cases. The packing fraction is computed via
// an initial evaluation of the number of spheres per unit length, dNdz, along the z axis, through a
// linear fit of N vs z to obtain the slope. The following variables are defined for this purpose.

double Sumz, Sumzz, N, Nplaced, MaxN, SumN, SumzN;
double Numer, Denom, dNdz;

// Using the variables below, the maximum values of dNdz in Regime 1, in Regime 2, in a chosen z position
// of the second sphere in Regime 2, and across all cases, are evaluated.

double MaxdNdzReg1;
double MaxdNdzReg2, MaxdNdzReg2i;
double MaxdNdz;

// Note that the overall maximum of dNdz is just the maximum out of these different variables. Following
// Eqs. (3.36) and (3.37), two functions are defined to facilitate a computation of the two different
// volume fractions from dNdz:

double VolFract (double dNdz, double D)
{
     return (u*2*dNdz) / (3*D*D);
}

double VolFractCylindRing (double dNdz, double D)
{
     return (u*dNdz) / (6*(D-1));
}

// The variables below are defined for the purpose of generating surface representations of spheres in each structure.
// The variables IncreDELsSurfRep and OutputMaxScale respectively set the resolution and scales of each
// surface representation.

double DELzSurfRep, DELsSurfRep, LimDELsSurfRep;
const double IncreDELsSurfRep = 0.05;

double RepeatSurfRep, AuxVar;
const double OutputMaxScale = 6;

// Apart from surface representations of spheres, the Cartesian coordinates of any close-packed structure
// generated from the present packing algorithm are also output via a use the variables below, with AuxVarInt
// being an auxiliary variable for the corresponding loop.

double x, y;
int AuxVarInt;

// A set of command variables are defined to control data output and monitor progress of the simulations,
// with "1" and "0" representing respectively execution and non-execution of a command:

// 1. Vary D across a finite range from MinD to (MinD + MaxCompStep x IncreD):

const int VaryD = 0;

// 2. Vary j2Trial to find the densest possible structure:

const int Varyj2TrialSingleD = 1;

// 3. If VaryD is equal to 1, Varyj2Trial is also equal to 1. And if VaryD is equal to 0, Varyj2Trial is
// equal to Varyj2TrialSingleD:

const int Varyj2Trial = VaryD + (1-VaryD)*Varyj2TrialSingleD;

// 4. If Varyj2Trial is equal to 0, j2 will take up the following value:

const int j2Chosen = 1000;

// 5. If j2Chosen is greater than or equal to DELjZrDELzContact, i.e. in Regime 2, i2 will take up the
// following value:

const int i2Chosen = 0;

// 6. Output VolFract against Theta2 for Regime 1:

const int OutputReg1VolFractvsTheta2Trial = 1 - VaryD;

// 7. Output MaxdNdzReg2i against z2Trial for Regime 2:

const int OutputReg2MaxdNdzReg2ivsz2Trial = 1 - VaryD;

// 8. Output related data of MaxVolFract at a single value of D:

const int OutputMaxVolFractData = 1 - VaryD;

// 9. Output MaxVolFract against D:

const int OutputMaxVolFractvsD = VaryD;

// 10. Monitor progress for Regime 1:

const int MonitorProgReg1 = 1 - VaryD;

// 11. Monitor progress for Regime 2:

const int MonitorProgReg2 = 1 - VaryD;

// 12. Output the surface representation and Cartesian coordinates of the structure of interest:

const int OutputStruct = 1 - VaryD;

// 13. Other commands:

int RunLoop, PlaceSphere, ComputedNdz, UpdateMaxdNdzReg2, OutputOtherCoors;


// Main function
int main()
{
	ofstream outfile("D=.txt", ios::out);

	// lower limit of D
	if (u==1)
	{
		LowLimD = 1;
	}

	else
	{
		LowLimD = 1 + sqrt(2/(1-cos(2*Pi/u)));
	}

    // 'maxCompStep' has to be zero if only one value of 'D' is used, i.e. if VaryD' is zero
	MaxCompStep *= VaryD;

	// description of data in the output file
	if (OutputMaxVolFractvsD==1)
	{
		outfile <<"Data of 'MaxVolFract' vs 'D': "<< endl;
        outfile <<"'D', 'VolFract(MaxdNdz,D)', 'VolFractCylindRing(MaxdNdz,D)', 'ThetaConv(j2MaxdNdz)', "
		<<"'ThetaConvDeg(j2MaxdNdz)', 'zConv(i2MaxdNdz)' "<< endl;

	    outfile << endl;
	}

	// looping across the chosen range of 'D'
	for (CompStep=0; CompStep<=MaxCompStep; ++CompStep)
	{
        // setting the value of 'D'
		D = MinD + ((double)CompStep)*IncreD;

		// if the diameter ratio D is too small
		if (D<LowLimD)
		{
			// Note: no allowed configuration for a simultaneous deposition of 'u' spheres
			// onto the same vertical position

			outfile <<"No allowed configuration for D = "<< D <<" at u = "<< u << endl;
			cout <<"No allowed configuration for D = "<< D <<" at u = "<< u << endl;
		}

		// otherwise
		else
		{
			// setting the value of 'MaxN' (in the repeating pattern) for computations of 'dNdz'
			UppLimu = 1;
			RunLoop = 1;

			do
			{
				UppLimu += 1;

				LowLimDTrial = 1 + sqrt(2/(1-cos(2*Pi/UppLimu)));

				if (LowLimDTrial>D)
				{
					// ending the loop
					RunLoop = 0;

					// returning to the original value
					UppLimu -= 1;
				}

			} while (RunLoop==1);

			MaxN = int((Maxz - 2*Thresholdz)*UppLimu/u);

			outfile <<"There will be "<< u*MaxN
			<<" spheres in the sphere column for volume fraction calculations"<< endl;

			outfile << endl;
	        outfile << endl;


			// description of data in the output file
			if (VaryD==0)
			{
				outfile <<"Data for D = "<< D <<" at a simultaneous deposition of u = "<< u <<" sphere(s)"<< endl;

				outfile << endl;
				outfile << endl;
			}


			// PART ONE - CALCULATION OF 'MaxDELjNzDELzContact' & 'ArrDELiContact[DELj]';

			// initial set-zero
			MaxDELjNzDELzContact = 0;

			// looping across all values of 'DELj'
			for (DELj=0; DELj<=MaxDELj; ++DELj)
			{
				// conversion into 'DELTheta'
				DELTheta = ThetaConv(DELj);

				// calculating 'SqDELzContact' (see Appendix 1)
				SqDELzContact = 1 - ( (D-1) * (D-1) * sin(DELTheta/2) * sin(DELTheta/2) );

				// initial set-zero
				DELiContact = 0;
				ArrDELiContact[DELj] = 0;

				if (SqDELzContact>0)
				{
					// calculating 'DELiContact'
					DELiContact = (int)( sqrt(SqDELzContact) * ((double)(ArrSizez)) / Maxz );
				}

				if (DELiContact>0)
				{
					// inputting this value of 'DELiContact' into its array
					ArrDELiContact[DELj] = DELiContact;

					// resetting 'MaxDELjNzDELzContact'
					MaxDELjNzDELzContact = DELj;
				}
			}

			// 'DELjZrDELzContact' only needs to be calculated after a final value of 'MaxDELjNzDELzContact'
            // is  obtained.
			DELjZrDELzContact = MaxDELjNzDELzContact + 1;

			// Note: If all the values of 'DELiContact' are non-zero, 'MaxDELjNzDELzContact' would be equal to
            // 'MaxDELj' such that 'DELjZrDELzContact' is equal to ('MaxDELj'+1). In this case, after the 1st set
		    // of 'u' spheres is placed, the 2nd set of 'u' spheres cannot be placed onto the flat bottom of the cylinder.


			// If 'j2Trial' is varied for a search of the densest possible structure
			if (Varyj2Trial==1)
			{
				// PART TWO - CASES OF 0 <= 'j2Trial' < 'DELjZrDELzContact' (Regime 1)

				// initial setting at an arbitrarily small value
				MaxdNdzReg1 = -1;

				LowLimj2MaxdNdzReg1 = -1;
				UppLimj2MaxdNdzReg1 = -1;

				LowLimi2MaxdNdzReg1 = -1;
				UppLimi2MaxdNdzReg1 = -1;

				// description of data in the output file
				if (OutputReg1VolFractvsTheta2Trial==1)
				{
					outfile <<"Regime 1 - 'VolFract' vs 'Theta2Trial': "<< endl;
					outfile <<"'j2Trial', 'ThetaConv(j2Trial)', 'ThetaConvDeg(j2Trial)', 'VolFract(dNdz,D)'"<< endl;

					outfile << endl;
				}

				// looping across all values of 'j2Trial' in regime 1
				for (j2Trial=0; j2Trial<DELjZrDELzContact; ++j2Trial)
				{
					// initial set-zero for 'ArrLowesti'
					for (j=0; j<ArrSizeTheta; ++j)
					{
						ArrLowesti[j] = 0;
					}

					// initial assumption that 'dNdz' is to be computed
					ComputedNdz = 1;

					// initial set-zero for computing 'dNdz':
					N = 0;
					Sumz = 0;
					Sumzz = 0;
					SumN = 0;
					SumzN = 0;

					// initial set-zero for 'Nplaced'
					Nplaced = 0;

					// placing the 1st set of 'u' spheres
					Nplaced += 1;

					Minj = 0;
					Theta = ThetaConv(Minj);

					Mini = 0;
					z = zConv(Mini);

					// updating 'ArrLowesti'
					DELj = 0;
					j = Minj;

					NewLowesti = Mini + ArrDELiContact[DELj];

					if (ArrLowesti[j]<NewLowesti)
					{
						ArrLowesti[j] = NewLowesti;
					}

					for (DELj=1; DELj<=MaxDELjNzDELzContact; ++DELj)
					{
						NewLowesti = Mini + ArrDELiContact[DELj];

						// 1st value of 'j'
						j = Minj - DELj;

						// keeping 'j' within the range {0,'ArrSizeTheta'-1}
						if (j<0)
						{
							j += ArrSizeTheta;
						}

						// updating 'ArrLowesti[j]'
						if (ArrLowesti[j]<NewLowesti)
						{
							ArrLowesti[j] = NewLowesti;
						}

						// 2nd value of 'j'
						j = Minj + DELj;

						// keeping 'j' within the range {0,'ArrSizeTheta'-1}
						if (j>(ArrSizeTheta-1))
						{
							j -= ArrSizeTheta;
						}

						// updating 'ArrLowesti[j]'
						if (ArrLowesti[j]<NewLowesti)
						{
							ArrLowesti[j] = NewLowesti;
						}
					}

					// placing the 2nd set of 'u' spheres
					Nplaced += 1;

					Minj = j2Trial;
					Theta = ThetaConv(Minj);

					PrevMini = Mini;
					Mini = ArrLowesti[Minj];
					i2 = Mini;
					z = zConv(Mini);

					// updating 'ArrLowesti'
					DELj = 0;
					j = Minj;

					NewLowesti = Mini + ArrDELiContact[DELj];

					if (ArrLowesti[j] < NewLowesti)
					{
						ArrLowesti[j] = NewLowesti;
					}

					for (DELj=1; DELj<=MaxDELjNzDELzContact; ++DELj)
					{
						NewLowesti = Mini + ArrDELiContact[DELj];

						// 1st value of 'j'
						j = Minj - DELj;

						// keeping 'j' within the range {0,'ArrSizeTheta'-1}
						if (j<0)
						{
							j += ArrSizeTheta;
						}

						// updating 'ArrLowesti[j]'
						if (ArrLowesti[j]<NewLowesti)
						{
							ArrLowesti[j] = NewLowesti;
						}

						// 2nd value of 'j'
						j = Minj + DELj;

						// keeping 'j' within the range {0,'ArrSizeTheta'-1}
						if (j>(ArrSizeTheta-1))
						{
							j -= ArrSizeTheta;
						}

						// updating 'ArrLowesti[j]'
						if (ArrLowesti[j]<NewLowesti)
						{
							ArrLowesti[j] = NewLowesti;
						}
					}

					// placing subsequent sets of 'u' spheres
					do // while 'PlaceSphere' is equal to 1
					{
						// initial assumption that no subsequent sets of spheres are to be placed
						PlaceSphere = 0;

						// finding 'minj'
						LowLimNewMinj = Minj;
						UppLimNewMinj = Minj;

						// looping from 'minj' to ('ArrSizeTheta'-1)
						for (j=Minj; j<ArrSizeTheta; ++j)
						{
							if (ArrLowesti[j]>0) // if not on the flat base
							{
								if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
								{
									LowLimNewMinj = j;

									// evaluating the corresponding 'UppLimNewMinj'
									UppLimNewMinj = LowLimNewMinj;

									do
									{
										UppLimNewMinj += 1;

										if (UppLimNewMinj>(ArrSizeTheta-1))
										{
											UppLimNewMinj -= ArrSizeTheta;
										}

									} while (ArrLowesti[UppLimNewMinj]==ArrLowesti[LowLimNewMinj]);

									UppLimNewMinj -= 1;

									if (UppLimNewMinj<0)
									{
										UppLimNewMinj += ArrSizeTheta;
									}
								}
							}

							else if (ArrLowesti[j]==0) // if on the flat base
							{
								if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
								{
									LowLimNewMinj = j;
									UppLimNewMinj = LowLimNewMinj;

									// Note: only taking into account the first lowest position in the angular scan,
									// since this lowest position generally spans across a finite range
								}
							}
						}

						// looping from 0 to ('minj'-1)
						for (j=0; j<Minj; ++j)
						{
							if (ArrLowesti[j]>0) // if not on the flat base
							{
								if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
								{
									LowLimNewMinj = j;

									// evaluating the corresponding 'UppLimNewMinj'
									UppLimNewMinj = LowLimNewMinj;

									do
									{
										UppLimNewMinj += 1;

										if (UppLimNewMinj>(ArrSizeTheta-1))
										{
											UppLimNewMinj -= ArrSizeTheta;
										}

									} while (ArrLowesti[UppLimNewMinj]==ArrLowesti[LowLimNewMinj]);

									UppLimNewMinj -= 1;

									if (UppLimNewMinj<0)
									{
										UppLimNewMinj += ArrSizeTheta;
									}
								}
							}

							else if (ArrLowesti[j]==0) // if on the flat base
							{
								if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
								{
									LowLimNewMinj = j;
									UppLimNewMinj = LowLimNewMinj;

									// Note: only taking into account the first lowest position in the angular scan,
									// since this lowest position generally spans across a finite range
								}
							}
						}

						if (LowLimNewMinj<=UppLimNewMinj)
						{
							NewMinj = (LowLimNewMinj + UppLimNewMinj) / 2;
						}

						else
						{
							NewMinj = ((LowLimNewMinj + UppLimNewMinj + ArrSizeTheta)/2);

							if (NewMinj>(ArrSizeTheta-1))
							{
								NewMinj -= ArrSizeTheta;
							}
						}

						Minj = NewMinj;

						// finding 'Mini'
						PrevMini = Mini;
						Mini = ArrLowesti[Minj];

						// resetting 'Mini' & 'Minj' if the sphere hits the flat bottom
						if  (Mini==0)
						{
							// Note: The new set of 'u' spheres, if they are to be placed, would be the
							// ('Nplaced'+1)th set of spheres in the structure.

							Minj = ((int)((Nplaced+1) - 1) * j2Trial)%(ArrSizeTheta-1);
							// based on a continuity in angular displacement

							Mini = ArrLowesti[Minj]; // must be non-zero (Regime 1 of 'j2Trial')
						}

						// conversion into z & Theta
						z = zConv(Mini);
						Theta = ThetaConv(Minj);

						// Note: The set of spheres are to be placed if 'Mini' is within the allowed range & if the
					    // set of spheres are to be placed at a position higher than that of the previously placed

						if ((Mini<ArrSizez) && (Mini>=PrevMini))
						{
							// placing the set of 'u' spheres
							PlaceSphere = 1;
							Nplaced += 1;

							// updating parameters for a computation of 'dNdz'
							if ((z>=Thresholdz) & (N<MaxN))
							{
								N += 1;
								Sumz += z;
								Sumzz += (z*z);
								SumN += N;
								SumzN += (z*N);
							}

							// updating 'ArrLowesti'
							DELj = 0;
							j = Minj;

							NewLowesti = Mini + ArrDELiContact[DELj];

							if (ArrLowesti[j]<NewLowesti)
							{
								ArrLowesti[j] = NewLowesti;
							}

							for (DELj=1; DELj<=MaxDELjNzDELzContact; ++DELj)
							{
								NewLowesti = Mini + ArrDELiContact[DELj];

								// 1st value of 'j'
								j = Minj - DELj;

								// keeping 'j' within the range {0,'ArrSizeTheta'-1}
								if (j<0)
								{
									j += ArrSizeTheta;
								}

								// updating 'ArrLowesti[j]'
								if (ArrLowesti[j]<NewLowesti)
								{
									ArrLowesti[j] = NewLowesti;
								}

								// 2nd value of 'j'
								j = Minj + DELj;

								// keeping 'j' within the range {0,'ArrSizeTheta'-1}
								if (j>(ArrSizeTheta-1))
								{
									j -= ArrSizeTheta;
								}

								// updating 'ArrLowesti[j]'
								if (ArrLowesti[j]<NewLowesti)
								{
									ArrLowesti[j] = NewLowesti;
								}
							}
						}

						else if (Mini<PrevMini)
						{
							// no need to compute 'dNdz'
							ComputedNdz = 0;

							// Note: because the set of spheres are to be placed at a position
							// lower than that of the previously placed
						}

					} while ((PlaceSphere==1));

					// computing 'dNdz' & updating 'MaxdNdzReg1'
					if ((ComputedNdz==1) && (N==MaxN))
					{
						Numer = (N*SumzN) - (Sumz*SumN);
						Denom = (N*Sumzz) - (Sumz*Sumz);
						dNdz = Numer / Denom;

						// outputting 'VolFract' vs 'Theta2Trial'
						if (OutputReg1VolFractvsTheta2Trial==1)
						{
							outfile << j2Trial <<" "<< ThetaConv(j2Trial) <<" "<< ThetaConvDeg(j2Trial)
								    <<" "<< VolFract(dNdz,D) << endl;
						}

						// lower & upper limits of 'j2MaxdNdzReg1'
						if (dNdz>=MaxdNdzReg1)
						{
							if (dNdz>MaxdNdzReg1)
							{
								LowLimj2MaxdNdzReg1 = j2Trial;
								LowLimi2MaxdNdzReg1 = i2;
							}

							if (dNdz>=MaxdNdzReg1)
							{
								UppLimj2MaxdNdzReg1 = j2Trial;
								UppLimi2MaxdNdzReg1 = i2;
							}

							// updating 'MaxdNdzReg1'
							MaxdNdzReg1 = dNdz;
						}
					}
				}

				// monitoring progress: completion of PART TWO (Regime 1)
				if (MonitorProgReg1==1)
				{
					cout <<"D = "<< D <<" - Calculations for Regime 1 completed."<< endl;

					outfile << endl;
					outfile << endl;
				}


				// PART THREE - CASES OF 'DELjZrDELzContact' <= 'j2Trial' <= 'MaxDELj' (Regime 2)

				// initial setting at an arbitrarily small value
				MaxdNdzReg2 = -1;

				LowLimj2MaxdNdzReg2 = -1;
				UppLimj2MaxdNdzReg2 = -1;

				LowLimi2MaxdNdzReg2 = -1;
				UppLimi2MaxdNdzReg2 = -1;

				if (DELjZrDELzContact<=MaxDELj)
				{
					// description of data in the output file
					if (OutputReg2MaxdNdzReg2ivsz2Trial==1)
					{
						outfile <<"Regime 2 - 'VolFract(MaxdNdzReg2i,D)' vs 'z2Trial':"<< endl;
						outfile <<"'i2Trial', 'zConv(i2Trial)', 'j2MaxdNdzReg2i', 'ThetaConv(j2MaxdNdzReg2i)', "
							    <<" ThetaConvDeg(j2MaxdNdzReg2i), 'VolFract(MaxdNdzReg2i,D)'"<< endl;

						outfile << endl;
					}

					// looping across a given range of i2Trial, where this range corresponds to
					// an equivalent height of a sphere diameter
					for (i2Trial=Mini2Trial; i2Trial<=Maxi2Trial; ++i2Trial)
					{
						// initial setting at an arbitrarily small value
						MaxdNdzReg2i = -1;

						LowLimj2MaxdNdzReg2i = -1;
						UppLimj2MaxdNdzReg2i = -1;

						// initial assumption that MaxdNdzReg2 is not to be updated
						UpdateMaxdNdzReg2 = 0;

						// looping across all values of 'j2Trial' for the corresponding value of 'i2Trial'
						for (j2Trial=DELjZrDELzContact; j2Trial<=MaxDELj; ++j2Trial)
						{
							// initial set-zero for 'ArrLowesti'
							for (j=0; j<ArrSizeTheta; ++j)
							{
								ArrLowesti[j] = 0;
							}

							// initial assumption that 'dNdz' is to be computed
							ComputedNdz = 1;

							// initial set-zero for computing 'dNdz'
							N = 0;
							Sumz = 0;
							Sumzz = 0;
							SumN = 0;
							SumzN = 0;

							// initial set-zero for 'Nplaced'
							Nplaced = 0;

							// placing the 1st set of 'u' spheres
							Nplaced += 1;

							Minj = 0;
							Theta = ThetaConv(Minj);

							Mini = 0;
							z = zConv(Mini);

							// updating 'ArrLowesti'
							DELj = 0;
							j = Minj;

							NewLowesti = Mini + ArrDELiContact[DELj];

							if (ArrLowesti[j]<NewLowesti)
							{
								ArrLowesti[j] = NewLowesti;
							}

							for (DELj=1; DELj<=MaxDELjNzDELzContact; ++DELj)
							{
								NewLowesti = Mini + ArrDELiContact[DELj];

								// 1st value of 'j'
								j = Minj - DELj;

								// keeping 'j' within the range {0,'ArrSizeTheta'-1}
								if (j<0)
								{
									j += ArrSizeTheta;
								}

								// updating 'ArrLowesti[j]'
								if (ArrLowesti[j]<NewLowesti)
								{
									ArrLowesti[j] = NewLowesti;
								}

								// 2nd value of 'j'
								j = Minj + DELj;

								// keeping 'j' within the range {0,'ArrSizeTheta'-1}
								if (j>(ArrSizeTheta-1))
								{
									j -= ArrSizeTheta;
								}

								// updating 'ArrLowesti[j]'
								if (ArrLowesti[j]<NewLowesti)
								{
									ArrLowesti[j] = NewLowesti;
								}
							}

							// placing the 2nd set of 'u' spheres
							Nplaced += 1;

							Minj = j2Trial;
							Theta = ThetaConv(Minj);

							PrevMini = Mini;
							Mini = i2Trial;
							i2 = Mini;
							z = zConv(Mini);

							// updating 'ArrLowesti'
							DELj = 0;
							j = Minj;

							NewLowesti = Mini + ArrDELiContact[DELj];

							if (ArrLowesti[j]<NewLowesti)
							{
								ArrLowesti[j] = NewLowesti;
							}

							for (DELj=1; DELj<=MaxDELjNzDELzContact; ++DELj)
							{
								NewLowesti = Mini + ArrDELiContact[DELj];

								// 1st value of 'j'
								j = Minj - DELj;

								// keeping 'j' within the range {0,'ArrSizeTheta'-1}
								if (j<0)
								{
									j += ArrSizeTheta;
								}

								// updating 'ArrLowesti[j]'
								if (ArrLowesti[j]<NewLowesti)
								{
									ArrLowesti[j] = NewLowesti;
								}

								// 2nd value of 'j'
								j = Minj + DELj;

								// keeping 'j' within the range {0,'ArrSizeTheta'-1}
								if (j>(ArrSizeTheta-1))
								{
									j -= ArrSizeTheta;
								}

								// updating 'ArrLowesti[j]'
								if (ArrLowesti[j]<NewLowesti)
								{
									ArrLowesti[j] = NewLowesti;
								}
							}

							// placing subsequent sets of 'u' spheres
							do // while 'PlaceSphere' is equal to 1
							{
								// initial assumption that no subsequent sets of spheres are to be placed
								PlaceSphere = 0;

								// finding 'Minj'
								LowLimNewMinj = Minj;
								UppLimNewMinj = Minj;

								// looping from 'Minj' to ('ArrSizeTheta'-1)
								for (j=Minj; j<ArrSizeTheta; ++j)
								{
									if (ArrLowesti[j]>0) // if not on the flat base
									{
										if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
										{
											LowLimNewMinj = j;

											// evaluating the corresponding 'UppLimNewMinj'
											UppLimNewMinj = LowLimNewMinj;

											do
											{
												UppLimNewMinj += 1;

												if (UppLimNewMinj>(ArrSizeTheta-1))
												{
													UppLimNewMinj -= ArrSizeTheta;
												}

											} while (ArrLowesti[UppLimNewMinj]==ArrLowesti[LowLimNewMinj]);

											UppLimNewMinj -= 1;

											if (UppLimNewMinj<0)
											{
												UppLimNewMinj += ArrSizeTheta;
											}
										}
									}

									else if (ArrLowesti[j]==0) // if on the flat base
									{
										if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
										{
											LowLimNewMinj = j;
											UppLimNewMinj = LowLimNewMinj;

											// Note: only taking into account the first lowest position in the
                                            // angular scan, since this lowest position generally spans across a
                                            // finite range

										}
									}
								}

								// looping from 0 to ('Minj'-1)
								for (j=0; j<Minj; ++j)
								{
									if (ArrLowesti[j]>0) // if not on the flat base
									{
										if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
										{
											LowLimNewMinj = j;

											// evaluating the corresponding 'UppLimNewMinj'
											UppLimNewMinj = LowLimNewMinj;

											do
											{
												UppLimNewMinj += 1;

												if (UppLimNewMinj>(ArrSizeTheta-1))
												{
													UppLimNewMinj -= ArrSizeTheta;
												}

											} while (ArrLowesti[UppLimNewMinj]==ArrLowesti[LowLimNewMinj]);

											UppLimNewMinj -= 1;

											if (UppLimNewMinj<0)
											{
												UppLimNewMinj += ArrSizeTheta;
											}
										}
									}

									else if (ArrLowesti[j]==0) // if on the flat base
									{
										if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
										{
											LowLimNewMinj = j;
											UppLimNewMinj = LowLimNewMinj;

											// Note: only taking into account the first lowest position in the angular scan,
											// since this lowest position generally spans across a finite range
										}
									}
								}

								if (LowLimNewMinj<=UppLimNewMinj)
								{
									NewMinj = (LowLimNewMinj + UppLimNewMinj) / 2;
								}

								else
								{
									NewMinj = ((LowLimNewMinj + UppLimNewMinj + ArrSizeTheta)/2);

									if (NewMinj>(ArrSizeTheta-1))
									{
										NewMinj -= ArrSizeTheta;
									}
								}

								Minj = NewMinj;

								// finding 'Mini'
								PrevMini = Mini;
								Mini = ArrLowesti[Minj];

								// resetting 'Mini' & 'Minj' if the sphere hits the flat bottom
								if  (Mini==0)
								{
									// Note: The new set of 'u' spheres, if they are to be placed, would be the
									// ('Nplaced'+1)th set of spheres in the structure.

									Minj = ((int)((Nplaced+1) - 1) * j2Trial)%(ArrSizeTheta-1);
									// based on a continuity in angular displacement

									Mini = ArrLowesti[Minj]; // could be zero (Regime 2 of 'j2Trial')

									if (Mini==0) // if equal to zero
									{
										Mini = (int)((Nplaced+1) - 1) * i2;
										// based on a continuity in vertical displacement
									}
								}

								// conversion into z & Theta
								z = zConv(Mini);
								Theta = ThetaConv(Minj);

								// Note: The set of spheres are to be placed if 'Mini' is within the allowed
								// range & if the set of spheres are to be placed at a position higher than
								// that of the previously placed

								if ((Mini<ArrSizez) && (Mini>=PrevMini))
								{
									// placing the set of 'u' spheres
									Nplaced += 1;
									PlaceSphere = 1;

									// updating parameters for a computation of 'dNdz'
									if ((z>=Thresholdz) & (N<MaxN))
									{
										N += 1;
										Sumz += z;
										Sumzz += (z*z);
										SumN += N;
										SumzN += (z*N);
									}

									// updating ArrLowesti
									DELj = 0;
									j = Minj;

									NewLowesti = Mini + ArrDELiContact[DELj];

									if (ArrLowesti[j]<NewLowesti)
									{
										ArrLowesti[j] = NewLowesti;
									}

									for (DELj=1; DELj<=MaxDELjNzDELzContact; ++DELj)
									{
										NewLowesti = Mini + ArrDELiContact[DELj];

										// 1st value of 'j'
										j = Minj - DELj;

										// keeping 'j' within the range {0,'ArrSizeTheta'-1}
										if (j<0)
										{
											j += ArrSizeTheta;
										}

										// updating 'ArrLowesti[j]'
										if (ArrLowesti[j]<NewLowesti)
										{
											ArrLowesti[j] = NewLowesti;
										}

										// 2nd value of 'j'
										j = Minj + DELj;

										// keeping 'j' within the range {0,'ArrSizeTheta'-1}
										if (j>(ArrSizeTheta-1))
										{
											j -= ArrSizeTheta;
										}

										// updating 'ArrLowesti[j]'
										if (ArrLowesti[j]<NewLowesti)
										{
											ArrLowesti[j] = NewLowesti;
										}
									}
								}

								else if (Mini<PrevMini)
								{
									// no need to compute 'dNdz'
									ComputedNdz = 0;

									// Note: because the set of spheres are to be placed at a position
									// lower than that of the previously placed
								}

							} while ((PlaceSphere==1));

							// computing 'dNdz' & updating 'MaxdNdzReg2i'
							if ((ComputedNdz==1) && (N==MaxN))
							{
								// 'MaxdNdzReg2' is to be updated because allowed configurations
								// exist for this value of 'i2Trial'
								UpdateMaxdNdzReg2 = 1;

								Numer = (N*SumzN) - (Sumz*SumN);
								Denom = (N*Sumzz) - (Sumz*Sumz);
								dNdz = Numer / Denom;

								// lower & upper limits of 'j2MaxdNdzReg2i'
								if (dNdz>=MaxdNdzReg2i)
								{
									if (dNdz>MaxdNdzReg2i)
									{
										LowLimj2MaxdNdzReg2i = j2Trial;
									}

									if (dNdz>=MaxdNdzReg2i)
									{
										UppLimj2MaxdNdzReg2i = j2Trial;
									}

									// updating ''MaxdNdzReg2i'
									MaxdNdzReg2i = dNdz;
								}
							}
						}

						// if 'MaxdNdzReg2' is to be updated
						if (UpdateMaxdNdzReg2==1)
						{
							// computing 'j2MaxdNdzReg2i'
							j2MaxdNdzReg2i = (LowLimj2MaxdNdzReg2i+UppLimj2MaxdNdzReg2i)/2;

							// monitoring progress of looping across values of 'i2Trial'
							if (MonitorProgReg2==1)
							{
								cout <<"D = "<< D <<"; i2Trial = "<< i2Trial <<"; VolFractMaxi = "
								<< VolFract(MaxdNdzReg2i,D) <<" at  j2MaxdNdzReg2i = "<< j2MaxdNdzReg2i
								<<" for N = "<< N <<" spheres"<< endl;
							}

							// outputting 'VolFract(MaxdNdzReg2i,D)' vs 'z2Trial'
							if (OutputReg2MaxdNdzReg2ivsz2Trial==1)
							{
								outfile << i2Trial <<" "<< zConv(i2Trial) <<" "<< j2MaxdNdzReg2i
								<<" "<< ThetaConv(j2MaxdNdzReg2i) <<" "<< ThetaConvDeg(j2MaxdNdzReg2i)
								<<" "<< VolFract(MaxdNdzReg2i,D) << endl;
							}

							// 'j2MaxdNdzReg2i' & 'i2MaxdNdzReg2' - lower & upper limits
							if (MaxdNdzReg2i>=MaxdNdzReg2)
							{
								if (MaxdNdzReg2i>MaxdNdzReg2)
								{
									LowLimi2MaxdNdzReg2 = i2Trial;

									LowLimj2MaxdNdzReg2 = LowLimj2MaxdNdzReg2i;
									UppLimj2MaxdNdzReg2 = UppLimj2MaxdNdzReg2i;
								}

								if (MaxdNdzReg2i>=MaxdNdzReg2)
								{
									UppLimi2MaxdNdzReg2 = i2Trial;

									if (LowLimj2MaxdNdzReg2i<LowLimj2MaxdNdzReg2)
									{
										LowLimj2MaxdNdzReg2 = LowLimj2MaxdNdzReg2i;
									}

									if (UppLimj2MaxdNdzReg2i>UppLimj2MaxdNdzReg2)
									{
										UppLimj2MaxdNdzReg2 = UppLimj2MaxdNdzReg2i;
									}
								}

								// updating 'MaxdNdzReg2'
								MaxdNdzReg2 = MaxdNdzReg2i;
							}
						}

						else
						{
							// monitoring progress of looping across values of 'i2Trial'
							if (MonitorProgReg2==1)
							{
								cout <<"No allowed configuration exists for i2Trial = "<< i2Trial << endl;
							}
						}
					}

					// monitoring progress: completion of PART THREE (Regime 2)
					if (MonitorProgReg2==1)
					{
						cout <<"D = "<< D <<" - Calculations for Regime 2 completed."<< endl;

						outfile << endl;
						outfile << endl;
					}
				}


				// PART FOUR - FINDING 'MaxdNdz', 'j2MaxdNdz' & 'i2MaxdNdz'

				// initial setting at an arbitrarily small value
				MaxdNdz = -1;
				j2MaxdNdz = -1;
				i2MaxdNdz = -1;

				if (MaxdNdzReg1>MaxdNdzReg2)
				{
					MaxdNdz = MaxdNdzReg1;

					j2MaxdNdz = (LowLimj2MaxdNdzReg1 + UppLimj2MaxdNdzReg1) / 2;
					i2MaxdNdz = (LowLimi2MaxdNdzReg1 + UppLimi2MaxdNdzReg1) / 2;
				}

				else if (MaxdNdzReg2>MaxdNdzReg1)
				{
					MaxdNdz = MaxdNdzReg2;

					j2MaxdNdz = (LowLimj2MaxdNdzReg2 + UppLimj2MaxdNdzReg2) / 2;
					i2MaxdNdz = (LowLimi2MaxdNdzReg2 + UppLimi2MaxdNdzReg2) / 2;
				}

				else
				{
					cout <<"Problem with degenerate densest structures. Not sure which one to output. D = "<< D << endl;
					cout <<"'MaxdNdzReg1' = " << MaxdNdzReg1 <<"; "<<"'MaxdNdzReg2' = "<< MaxdNdzReg2 << endl;
				}

				// outputting 'D', 'VolFract(MaxdNdz,D)', 'VolFractCylindRing(MaxdNdz,D)',
				// 'j2MaxdNdz', 'Theta2MaxdNdz', 'Theta2MaxdNdz' (Deg), 'i2MaxdNdz', 'z2MaxdNdz'
				if (MaxdNdz>-1)
				{
					if (OutputMaxVolFractData==1)
					{
						outfile <<"'MaxVolFract' data: "<< endl;

						outfile <<"'VolFract(MaxdNdz,D)', 'VolFractCylindRing(MaxdNdz,D)', "<< endl;
						outfile <<"'j2MaxdNdz', 'ThetaConv(j2MaxdNdz)', 'ThetaConvDeg(j2MaxdNdz)', "<< endl;
						outfile <<"'i2MaxdNdz', 'zConv(i2MaxdNdz)' " << endl;

						outfile << endl;

						outfile << VolFract(MaxdNdz,D) <<" "<< VolFractCylindRing(MaxdNdz,D) <<" "
							    << j2MaxdNdz <<" "<< ThetaConv(j2MaxdNdz) <<" "<< ThetaConvDeg(j2MaxdNdz) <<" "
								<< i2MaxdNdz <<" "<< zConv(i2MaxdNdz) << endl;

						outfile << endl;
						outfile << endl;
					}

					if (OutputMaxVolFractvsD==1)
					{
						outfile << D <<" "<< VolFract(MaxdNdz,D) <<" "<< VolFractCylindRing(MaxdNdz,D) <<" "
							    << ThetaConv(j2MaxdNdz) <<" "<< ThetaConvDeg(j2MaxdNdz) <<" "<< zConv(i2MaxdNdz) << endl;
					}
				}
			}


			// PART FIVE - OUTPUTTING SURFACE REPRESENTATIONS

			if (OutputStruct==1)
			{
				// setting the upper limit of 'DELsSurfRep'
				LimDELsSurfRep = ((D-1)/2) * (MaxTheta/4); // corresponding to an angular separation of 'MaxTheta'/4

				// setting the values of 'j2Output' & 'i2Output'
				if (Varyj2TrialSingleD==1) // or equivalently if (Varyj2Trial==1)
				{
					if (j2MaxdNdz>=DELjZrDELzContact)
					{
						outfile <<"Structure (surface representations) of 'j2MaxdNdz' = "<< j2MaxdNdz <<" & 'i2MaxdNdz' = "<< i2MaxdNdz <<", in Regime 2: "<< endl;
					}

					else
					{
						outfile <<"Structure (surface representations) of 'j2MaxdNdz' = "<< j2MaxdNdz <<" & 'i2MaxdNdz' = "<< i2MaxdNdz <<", in Regime 1: "<< endl;
					}

					outfile <<"'s+-DELsSurfRep', 'z+-DELzSurfRep', 's', 'z'"<< endl;

				    outfile << endl;

					j2Output = j2MaxdNdz;
					i2Output = i2MaxdNdz;
				}

				else
				{
					if (j2Chosen>=DELjZrDELzContact)
					{
						outfile <<"Structure (surface patterns) of 'j2Chosen' = "<< j2Chosen <<" & 'i2Chosen' = "<< i2Chosen <<", in Regime 2: "<< endl;

						j2Output = j2Chosen;
						i2Output = i2Chosen;
					}

					else
					{
					    outfile <<"Structure (surface patterns) of 'j2Chosen' = "<< j2Chosen <<" & 'i2Contact' = "<< ArrDELiContact[j2Chosen] <<", in Regime 1: "<< endl;

						j2Output = j2Chosen;
						i2Output = ArrDELiContact[j2Chosen];
					}

					outfile <<"'s+-DELsSurfRep', 'z+-DELzSurfRep', 's', 'z'"<< endl;

					outfile << endl;
				}

				// initial set-zero for 'ArrLowesti'
				for (j=0; j<ArrSizeTheta; ++j)
				{
					ArrLowesti[j] = 0;
				}

				// initial set-zero for 'Nplaced'
				Nplaced = 0;

				// placing the 1st set of 'u' spheres
				Nplaced += 1;

				Minj = 0;
				Theta = ThetaConv(Minj);

				Mini = 0;
				z = zConv(Mini);

				// outputting the structure
				RepeatSurfRep = -1;

				do
				{
					s = ((D-1)/2) * (Theta+RepeatSurfRep*MaxTheta);

					if (s<2*OutputMaxScale)
					{
						DELsSurfRep = 0;
						OutputOtherCoors = 1; // only outputting some other coordinates when 'DELsSurfRep' is zero

						do
						{
							AuxVar = 0.5 * (D-1) * (D-1) * (1-cos(4*DELsSurfRep/(D-1)));

							if (AuxVar<1)
							{
								DELzSurfRep = 0.5 * sqrt(1 - AuxVar);

								if (OutputOtherCoors==1)
								{
									outfile << (s+DELsSurfRep) <<" "<< (z+DELzSurfRep) <<" "<< s <<" "<< z << endl;
								}

								else
								{
									outfile << (s+DELsSurfRep) <<" "<< (z+DELzSurfRep) << endl;
								}

								outfile << (s+DELsSurfRep) <<" "<< (z-DELzSurfRep) << endl;
								outfile << (s-DELsSurfRep) <<" "<< (z+DELzSurfRep) << endl;
								outfile << (s-DELsSurfRep) <<" "<< (z-DELzSurfRep) << endl;
							}

							DELsSurfRep += IncreDELsSurfRep;

							OutputOtherCoors = 0;

						} while (DELsSurfRep<=LimDELsSurfRep);

				        if (DELjZrDELzContact>MaxDELj)
						{
							DELsSurfRep = LimDELsSurfRep;
							AuxVar = 0.5 * (D-1) * (D-1) * (1-cos(4*DELsSurfRep/(D-1)));
							DELzSurfRep = 0.5 * sqrt(1 - AuxVar);

							do
							{
								outfile << (s+DELsSurfRep) <<" "<< (z+DELzSurfRep) << endl;
								outfile << (s+DELsSurfRep) <<" "<< (z-DELzSurfRep) << endl;
								outfile << (s-DELsSurfRep) <<" "<< (z+DELzSurfRep) << endl;
								outfile << (s-DELsSurfRep) <<" "<< (z-DELzSurfRep) << endl;

								DELzSurfRep -= IncreDELsSurfRep;

							} while (DELzSurfRep>0);
						}
					}

					RepeatSurfRep += 1;

				} while (s<2*OutputMaxScale);

				// updating 'ArrLowesti'
				DELj = 0;
				j = Minj;

				NewLowesti = Mini + ArrDELiContact[DELj];

				if (ArrLowesti[j]<NewLowesti)
				{
					ArrLowesti[j] = NewLowesti;
				}

				for (DELj=1; DELj<=MaxDELjNzDELzContact; ++DELj)
				{
					NewLowesti = Mini + ArrDELiContact[DELj];

					// 1st value of 'j'
					j = Minj - DELj;

					// keeping 'j' within the range {0,'ArrSizeTheta'-1}
					if (j<0)
					{
						j += ArrSizeTheta;
					}

					// updating 'ArrLowesti[j]'
					if (ArrLowesti[j]<NewLowesti)
					{
						ArrLowesti[j] = NewLowesti;
					}

					// 2nd value of 'j'
					j = Minj + DELj;

					// keeping 'j' within the range {0,'ArrSizeTheta'-1}
					if (j>(ArrSizeTheta-1))
					{
						j -= ArrSizeTheta;
					}

					// updating 'ArrLowesti[j]'
					if (ArrLowesti[j]<NewLowesti)
					{
						ArrLowesti[j] = NewLowesti;
					}
				}

				// placing the 2nd set of 'u' spheres
				Nplaced += 1;

				Minj = j2Output;
				Theta = ThetaConv(Minj);

				PrevMini = Mini;
				Mini = i2Output;
				i2 = Mini;
				z = zConv(Mini);

				// outputting the structure
				RepeatSurfRep = -1;

				do
				{
					s = ((D-1)/2) * (Theta+RepeatSurfRep*MaxTheta);

					if (s<2*OutputMaxScale)
					{
						DELsSurfRep = 0;
						OutputOtherCoors = 1; // only outputting some other coordinates when 'DELsSurfRep' is zero

						do
						{
							AuxVar = 0.5 * (D-1) * (D-1) * (1-cos(4*DELsSurfRep/(D-1)));

							if (AuxVar<1)
							{
								DELzSurfRep = 0.5 * sqrt(1 - AuxVar);

								if (OutputOtherCoors==1)
								{
									outfile << (s+DELsSurfRep) <<" "<< (z+DELzSurfRep) <<" "<< s <<" "<< z << endl;
								}

								else
								{
									outfile << (s+DELsSurfRep) <<" "<< (z+DELzSurfRep) << endl;
								}

								outfile << (s+DELsSurfRep) <<" "<< (z-DELzSurfRep) << endl;
								outfile << (s-DELsSurfRep) <<" "<< (z+DELzSurfRep) << endl;
								outfile << (s-DELsSurfRep) <<" "<< (z-DELzSurfRep) << endl;
							}

							DELsSurfRep += IncreDELsSurfRep;
							OutputOtherCoors = 0;

						} while (DELsSurfRep<=LimDELsSurfRep);

				        if (DELjZrDELzContact>MaxDELj)
						{
							DELsSurfRep = LimDELsSurfRep;
							AuxVar = 0.5 * (D-1) * (D-1) * (1-cos(4*DELsSurfRep/(D-1)));
							DELzSurfRep = 0.5 * sqrt(1 - AuxVar);

							do
							{
								outfile << (s+DELsSurfRep) <<" "<< (z+DELzSurfRep) << endl;
								outfile << (s+DELsSurfRep) <<" "<< (z-DELzSurfRep) << endl;
								outfile << (s-DELsSurfRep) <<" "<< (z+DELzSurfRep) << endl;
								outfile << (s-DELsSurfRep) <<" "<< (z-DELzSurfRep) << endl;

								DELzSurfRep -= IncreDELsSurfRep;

							} while (DELzSurfRep>0);
						}
					}

					RepeatSurfRep += 1;

				} while (s<2*OutputMaxScale);

				// updating 'ArrLowesti'
				DELj = 0;
				j = Minj;

				NewLowesti = Mini + ArrDELiContact[DELj];

				if (ArrLowesti[j]<NewLowesti)
				{
					ArrLowesti[j] = NewLowesti;
				}

				for (DELj=1; DELj<=MaxDELjNzDELzContact; ++DELj)
				{
					NewLowesti = Mini + ArrDELiContact[DELj];

					// 1st value of 'j'
					j = Minj - DELj;

					// keeping 'j' within the range {0,'ArrSizeTheta'-1}
					if (j<0)
					{
						j += ArrSizeTheta;
					}

					// updating 'ArrLowesti[j]'
					if (ArrLowesti[j]<NewLowesti)
					{
						ArrLowesti[j] = NewLowesti;
					}

					// 2nd value of 'j'
					j = Minj + DELj;

					// keeping 'j' within the range {0,'ArrSizeTheta'-1}
					if (j>(ArrSizeTheta-1))
					{
						j -= ArrSizeTheta;
					}

					// updating 'ArrLowesti[j]'
					if (ArrLowesti[j]<NewLowesti)
					{
						ArrLowesti[j] = NewLowesti;
					}
				}

				// placing subsequent sets of 'u' spheres
				do // while 'PlaceSphere' is equal to 1
				{
					// initial assumption that no subsequent sets of spheres are to be placed
					PlaceSphere = 0;

					// finding 'Minj'
					LowLimNewMinj = Minj;
					UppLimNewMinj = Minj;

					for (j=Minj; j<ArrSizeTheta; ++j) // looping from 'Minj' to ('ArrSizeTheta'-1)
					{
						if (ArrLowesti[j]>0) // if not on the flat base
						{
							if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
							{
								LowLimNewMinj = j;

								// evaluation of the corresponding 'UppLimNewMinj'
								UppLimNewMinj = LowLimNewMinj;

								do
								{
									UppLimNewMinj += 1;

									if (UppLimNewMinj>(ArrSizeTheta-1))
									{
										UppLimNewMinj -= ArrSizeTheta;
									}

								} while (ArrLowesti[UppLimNewMinj]==ArrLowesti[LowLimNewMinj]);

								UppLimNewMinj -= 1;

								if (UppLimNewMinj<0)
								{
									UppLimNewMinj += ArrSizeTheta;
								}
							}
						}

						else if (ArrLowesti[j]==0) // if on the flat base
						{
							if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
							{
								LowLimNewMinj = j;
								UppLimNewMinj = LowLimNewMinj;

								// Note: only taking into account the first lowest position in the angular scan,
								// since this lowest position generally spans across a finite range
							}
						}
					}

					for (j=0; j<Minj; ++j) // looping from 0 to ('Minj'-1)
					{
						if (ArrLowesti[j]>0) // if not on the flat base
						{
							if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
							{
								LowLimNewMinj = j;

								// evaluation of the corresponding 'UppLimNewMinj'
								UppLimNewMinj = LowLimNewMinj;

								do
								{
									UppLimNewMinj += 1;

									if (UppLimNewMinj>(ArrSizeTheta-1))
									{
										UppLimNewMinj -= ArrSizeTheta;
									}

								} while (ArrLowesti[UppLimNewMinj]==ArrLowesti[LowLimNewMinj]);

								UppLimNewMinj -= 1;

								if (UppLimNewMinj<0)
								{
									UppLimNewMinj += ArrSizeTheta;
								}
							}
						}

						else if (ArrLowesti[j]==0) // if on the flat base
						{
							if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
							{
								LowLimNewMinj = j;
								UppLimNewMinj = LowLimNewMinj;

								// Note: only taking into account the first lowest position in the angular scan,
								// since this lowest position generally spans across a finite range
							}
						}
					}

					if (LowLimNewMinj<=UppLimNewMinj)
					{
						NewMinj = (LowLimNewMinj + UppLimNewMinj) / 2;
					}

					else
					{
						NewMinj = ((LowLimNewMinj + UppLimNewMinj + ArrSizeTheta)/2);

						if (NewMinj>(ArrSizeTheta-1))
						{
							NewMinj -= ArrSizeTheta;
						}
					}

					Minj = NewMinj;

					// finding 'Mini'
					PrevMini = Mini;
					Mini = ArrLowesti[Minj];

					// resetting 'Mini' & 'Minj' if the sphere hits the flat bottom
					if  (Mini==0)
					{
						// Note: The new set of 'u' spheres, if they are to be placed, would be the
						// ('Nplaced'+1)th set of spheres in the structure.

						Minj = ((int)((Nplaced+1) - 1) * j2Output)%(ArrSizeTheta-1);

						Mini = ArrLowesti[Minj];

						if (Mini==0) // depending on whether 'D' is greater than a critical value
						{
							Mini = (int)((Nplaced+1) - 1) * i2; // based on a continuity in vertical displacement
						}
					}

					// conversion into z & Theta
					z = zConv(Mini);
					Theta = ThetaConv(Minj);

					// Note: The set of spheres are to be placed if 'Mini' is within the allowed range & if the
					// set of spheres are to be placed at a position higher than that of the previously placed

					if ((Mini<ArrSizez) && (Mini>=PrevMini))
					{
						// placing the set of 'u' spheres
						PlaceSphere = 1;
						Nplaced += 1;

						// Outputting the structure
						RepeatSurfRep = -1;

						do
						{
							s = ((D-1)/2) * (Theta+RepeatSurfRep*MaxTheta);

							if (s<2*OutputMaxScale)
							{
								DELsSurfRep = 0;
								OutputOtherCoors = 1; // only outputting some other coordinates when 'DELsSurfRep' is zero

								do
								{
									AuxVar = 0.5 * (D-1) * (D-1) * (1-cos(4*DELsSurfRep/(D-1)));

									if (AuxVar<1)
									{
										DELzSurfRep = 0.5 * sqrt(1 - AuxVar);

										if (OutputOtherCoors==1)
										{
											outfile << (s+DELsSurfRep) <<" "<< (z+DELzSurfRep) <<" "<< s <<" "<< z << endl;
										}

										else
										{
											outfile << (s+DELsSurfRep) <<" "<< (z+DELzSurfRep) << endl;
										}

										outfile << (s+DELsSurfRep) <<" "<< (z-DELzSurfRep) << endl;
										outfile << (s-DELsSurfRep) <<" "<< (z+DELzSurfRep) << endl;
										outfile << (s-DELsSurfRep) <<" "<< (z-DELzSurfRep) << endl;
									}

									DELsSurfRep += IncreDELsSurfRep;
									OutputOtherCoors = 0;

								} while (DELsSurfRep<=LimDELsSurfRep);

							    if (DELjZrDELzContact>MaxDELj)
								{
									DELsSurfRep = LimDELsSurfRep;
									AuxVar = 0.5 * (D-1) * (D-1) * (1-cos(4*DELsSurfRep/(D-1)));
									DELzSurfRep = 0.5 * sqrt(1 - AuxVar);

									do
									{
										outfile << (s+DELsSurfRep) <<" "<< (z+DELzSurfRep) << endl;
										outfile << (s+DELsSurfRep) <<" "<< (z-DELzSurfRep) << endl;
										outfile << (s-DELsSurfRep) <<" "<< (z+DELzSurfRep) << endl;
										outfile << (s-DELsSurfRep) <<" "<< (z-DELzSurfRep) << endl;

										DELzSurfRep -= IncreDELsSurfRep;

									} while (DELzSurfRep>0);
								}
							}

							RepeatSurfRep += 1;

						} while (s<2*OutputMaxScale);

						// updating 'ArrLowesti'
						DELj = 0;
						j = Minj;

						NewLowesti = Mini + ArrDELiContact[DELj];

						if (ArrLowesti[j]<NewLowesti)
						{
							ArrLowesti[j] = NewLowesti;
						}

						for (DELj=1; DELj<=MaxDELjNzDELzContact; ++DELj)
						{
							NewLowesti = Mini + ArrDELiContact[DELj];

							// 1st value of 'j'
							j = Minj - DELj;

							// keeping 'j' within the range {0,'ArrSizeTheta'-1}
							if (j<0)
							{
								j += ArrSizeTheta;
							}

							// updating 'ArrLowesti[j]'
							if (ArrLowesti[j]<NewLowesti)
							{
								ArrLowesti[j] = NewLowesti;
							}

							// 2nd value of 'j'
							j = Minj + DELj;

							// keeping 'j' within the range {0,'ArrSizeTheta'-1}
							if (j>(ArrSizeTheta-1))
							{
								j -= ArrSizeTheta;
							}

							// updating 'ArrLowesti[j]'
							if (ArrLowesti[j]<NewLowesti)
							{
								ArrLowesti[j] = NewLowesti;
							}
						}
					}

				} while ((PlaceSphere==1) );

				outfile << endl;
				outfile << endl;
			}


			// PART SIX - OUTPUTTING CARTESIAN COORDINATES

			if (OutputStruct==1)
			{

				// setting the values of 'j2Output' & 'i2Output'
				if (Varyj2TrialSingleD==1) // or equivalently if (Varyj2Trial==1)
				{
					if (j2MaxdNdz>=DELjZrDELzContact)
					{
						outfile <<"Structure (Cartesian coordinates) of 'j2MaxdNdz' = "<< j2MaxdNdz <<" & 'i2MaxdNdz' = "<< i2MaxdNdz <<", in Regime 2: "<< endl;
					}

					else
					{
						outfile <<"Structure (Cartesian coordinates) of 'j2MaxdNdz' = "<< j2MaxdNdz <<" & 'i2MaxdNdz' = "<< i2MaxdNdz <<", in Regime 1: "<< endl;
					}

					outfile <<"'x', 'y', 'z' "<< endl;
				    outfile << endl;

					j2Output = j2MaxdNdz;
					i2Output = i2MaxdNdz;
				}

				else
				{
					if (j2Chosen>=DELjZrDELzContact)
					{
						outfile <<"Structure (Cartesian coordinates) of 'j2Chosen' = "<< j2Chosen <<" & 'i2Chosen' = "<< i2Chosen <<", in Regime 2: "<< endl;

						j2Output = j2Chosen;
						i2Output = i2Chosen;
					}

					else
					{
					    outfile <<"Structure (Cartesian coordinates) of 'j2Chosen' = "<< j2Chosen <<" & 'i2Contact' = "<< ArrDELiContact[j2Chosen] <<", in Regime 1: "<< endl;

						j2Output = j2Chosen;
						i2Output = ArrDELiContact[j2Chosen];
					}

					outfile <<"'x', 'y', 'z'"<< endl;
					outfile << endl;
				}

				// initial set-zero for 'ArrLowesti'
				for (j=0; j<ArrSizeTheta; ++j)
				{
					ArrLowesti[j] = 0;
				}

				// initial set-zero for 'Nplaced'
				Nplaced = 0;

				// placing the 1st set of 'u' spheres
				Nplaced += 1;

				Minj = 0;
				Theta = ThetaConv(Minj);

				Mini = 0;
				z = zConv(Mini);

				// outputting Cartesian coordinates
				for (AuxVarInt=0; AuxVarInt<u; ++AuxVarInt)
				{
					x = ((D-1)/2) * cos(Theta + ((double)AuxVarInt)*MaxTheta);
					y = ((D-1)/2) * sin(Theta + ((double)AuxVarInt)*MaxTheta);

					// outputting the Cartesian coordinates
					outfile << x <<" "<< y <<" "<< z << endl;
				}

				// updating 'ArrLowesti'
				DELj = 0;
				j = Minj;

				NewLowesti = Mini + ArrDELiContact[DELj];

				if (ArrLowesti[j]<NewLowesti)
				{
					ArrLowesti[j] = NewLowesti;
				}

				for (DELj=1; DELj<=MaxDELjNzDELzContact; ++DELj)
				{
					NewLowesti = Mini + ArrDELiContact[DELj];

					// 1st value of 'j'
					j = Minj - DELj;

					// keeping 'j' within the range {0,'ArrSizeTheta'-1}
					if (j<0)
					{
						j += ArrSizeTheta;
					}

					// updating 'ArrLowesti[j]'
					if (ArrLowesti[j]<NewLowesti)
					{
						ArrLowesti[j] = NewLowesti;
					}

					// 2nd value of 'j'
					j = Minj + DELj;

					// keeping 'j' within the range {0,'ArrSizeTheta'-1}
					if (j>(ArrSizeTheta-1))
					{
						j -= ArrSizeTheta;
					}

					// updating 'ArrLowesti[j]'
					if (ArrLowesti[j]<NewLowesti)
					{
						ArrLowesti[j] = NewLowesti;
					}
				}

				// placing the 2nd set of 'u' spheres
				Nplaced += 1;

				Minj = j2Output;
				Theta = ThetaConv(Minj);

				PrevMini = Mini;
				Mini = i2Output;
				i2 = Mini;
				z = zConv(Mini);

				// outputting Cartesian coordinates
				for (AuxVarInt=0; AuxVarInt<u; ++AuxVarInt)
				{
					x = ((D-1)/2) * cos(Theta + ((double)AuxVarInt)*MaxTheta);
					y = ((D-1)/2) * sin(Theta + ((double)AuxVarInt)*MaxTheta);

					// outputting the Cartesian coordinates
					outfile << x <<" "<< y <<" "<< z << endl;
				}

				// updating 'ArrLowesti'
				DELj = 0;
				j = Minj;

				NewLowesti = Mini + ArrDELiContact[DELj];

				if (ArrLowesti[j]<NewLowesti)
				{
					ArrLowesti[j] = NewLowesti;
				}

				for (DELj=1; DELj<=MaxDELjNzDELzContact; ++DELj)
				{
					NewLowesti = Mini + ArrDELiContact[DELj];

					// 1st value of 'j'
					j = Minj - DELj;

					// keeping 'j' within the range {0,'ArrSizeTheta'-1}
					if (j<0)
					{
						j += ArrSizeTheta;
					}

					// updating 'ArrLowesti[j]'
					if (ArrLowesti[j]<NewLowesti)
					{
						ArrLowesti[j] = NewLowesti;
					}

					// 2nd value of 'j'
					j = Minj + DELj;

					// keeping 'j' within the range {0,'ArrSizeTheta'-1}
					if (j>(ArrSizeTheta-1))
					{
						j -= ArrSizeTheta;
					}

					// updating 'ArrLowesti[j]'
					if (ArrLowesti[j]<NewLowesti)
					{
						ArrLowesti[j] = NewLowesti;
					}
				}

				// placing subsequent sets of 'u' spheres
				do // while 'PlaceSphere' is equal to 1
				{
					// initial assumption that no subsequent sets of spheres are to be placed
					PlaceSphere = 0;

					// finding 'Minj'
					LowLimNewMinj = Minj;
					UppLimNewMinj = Minj;

					for (j=Minj; j<ArrSizeTheta; ++j) // looping from 'Minj' to ('ArrSizeTheta'-1)
					{
						if (ArrLowesti[j]>0) // if not on the flat base
						{
							if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
							{
								LowLimNewMinj = j;

								// evaluation of the corresponding 'UppLimNewMinj'
								UppLimNewMinj = LowLimNewMinj;

								do
								{
									UppLimNewMinj += 1;

									if (UppLimNewMinj>(ArrSizeTheta-1))
									{
										UppLimNewMinj -= ArrSizeTheta;
									}

								} while (ArrLowesti[UppLimNewMinj]==ArrLowesti[LowLimNewMinj]);

								UppLimNewMinj -= 1;

								if (UppLimNewMinj<0)
								{
									UppLimNewMinj += ArrSizeTheta;
								}
							}
						}

						else if (ArrLowesti[j]==0) // if on the flat base
						{
							if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
							{
								LowLimNewMinj = j;
								UppLimNewMinj = LowLimNewMinj;

								// Note: only taking into account the first lowest position in the angular scan,
								// since this lowest position generally spans across a finite range
							}
						}
					}

					for (j=0; j<Minj; ++j) // looping from 0 to ('Minj'-1)
					{
						if (ArrLowesti[j]>0) // if not on the flat base
						{
							if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
							{
								LowLimNewMinj = j;

								// evaluation of the corresponding 'UppLimNewMinj'
								UppLimNewMinj = LowLimNewMinj;

								do
								{
									UppLimNewMinj += 1;

									if (UppLimNewMinj>(ArrSizeTheta-1))
									{
										UppLimNewMinj -= ArrSizeTheta;
									}

								} while (ArrLowesti[UppLimNewMinj]==ArrLowesti[LowLimNewMinj]);

								UppLimNewMinj -= 1;

								if (UppLimNewMinj<0)
								{
									UppLimNewMinj += ArrSizeTheta;
								}
							}
						}

						else if (ArrLowesti[j]==0) // if on the flat base
						{
							if (ArrLowesti[j]<ArrLowesti[LowLimNewMinj])
							{
								LowLimNewMinj = j;
								UppLimNewMinj = LowLimNewMinj;

								// Note: only taking into account the first lowest position in the angular scan,
								// since this lowest position generally spans across a finite range
							}
						}
					}

					if (LowLimNewMinj<=UppLimNewMinj)
					{
						NewMinj = (LowLimNewMinj + UppLimNewMinj) / 2;
					}

					else
					{
						NewMinj = ((LowLimNewMinj + UppLimNewMinj + ArrSizeTheta)/2);

						if (NewMinj>(ArrSizeTheta-1))
						{
							NewMinj -= ArrSizeTheta;
						}
					}

					Minj = NewMinj;

					// finding 'Mini'
					PrevMini = Mini;
					Mini = ArrLowesti[Minj];

					// resetting 'Mini' & 'Minj' if the sphere hits the flat bottom
					if  (Mini==0)
					{
						// Note: The new set of 'u' spheres, if they are to be placed, would be the
					    // ('Nplaced'+1)th set of spheres in the structure.

						Minj = ((int)((Nplaced+1) - 1) * j2Output)%(ArrSizeTheta-1);

						Mini = ArrLowesti[Minj];

						if (Mini==0) // depending on whether 'D' is greater than a critical value
						{
							Mini = (int)((Nplaced+1) - 1) * i2; // based on a continuity in vertical displacement
						}
					}

					// conversion into z & Theta
					z = zConv(Mini);
					Theta = ThetaConv(Minj);

					// Note: The set of spheres are to be placed if 'Mini' is within the allowed
					// range & if the set of spheres are to be placed at a position higher than
					// that of the previously placed

					if ((Mini<ArrSizez) && (Mini>=PrevMini))
					{
						// placing the set of 'u' spheres
						Nplaced += 1;
						PlaceSphere = 1;

						// outputting Cartesian coordinates
						for (AuxVarInt=0; AuxVarInt<u; ++AuxVarInt)
						{
							x = ((D-1)/2) * cos(Theta + ((double)AuxVarInt)*MaxTheta);
							y = ((D-1)/2) * sin(Theta + ((double)AuxVarInt)*MaxTheta);

							// outputting the Cartesian coordinates
							outfile << x <<" "<< y <<" "<< z << endl;
						}

						// updating 'ArrLowesti'
						DELj = 0;
						j = Minj;

						NewLowesti = Mini + ArrDELiContact[DELj];

						if (ArrLowesti[j]<NewLowesti)
						{
							ArrLowesti[j] = NewLowesti;
						}

						for (DELj=1; DELj<=MaxDELjNzDELzContact; ++DELj)
						{
							NewLowesti = Mini + ArrDELiContact[DELj];

							// 1st value of 'j'
							j = Minj - DELj;

							// keeping 'j' within the range {0,'ArrSizeTheta'-1}
							if (j<0)
							{
								j += ArrSizeTheta;
							}

							// updating 'ArrLowesti[j]'
							if (ArrLowesti[j]<NewLowesti)
							{
								ArrLowesti[j] = NewLowesti;
							}

							// 2nd value of 'j'
							j = Minj + DELj;

							// keeping 'j' within the range {0,'ArrSizeTheta'-1}
							if (j>(ArrSizeTheta-1))
							{
								j -= ArrSizeTheta;
							}

							// updating 'ArrLowesti[j]'
							if (ArrLowesti[j]<NewLowesti)
							{
								ArrLowesti[j] = NewLowesti;
							}
						}
					}

				} while ((PlaceSphere==1) );
			}

			// monitoring progress: calculation completed
			cout <<"calculation completed for D = "<< D << endl;
		}
	}

	outfile.close();
}


